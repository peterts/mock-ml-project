from src.news_classifier import NewsClassifier
import numpy as np
import pytest


@pytest.fixture(scope="module")
def news_classifier():
    return NewsClassifier()


def test_correct_output(news_classifier):
    """
    Test that the correct output type is given for a certain input type.

    Args:
        news_classifier (NewsClassifier): The classifier

    Returns:
        None
    """

    assert isinstance(news_classifier.classify("My windows are dirty"), str)
    assert isinstance(news_classifier.classify(["My windows are dirty"]), list)


def test_news_classifier(news_classifier):
    """
    Test that the news classifier successfully misinterprets the given text.

    Args:
        news_classifier (NewsClassifier): The classifier

    Returns:
        None
    """

    topic = news_classifier.classify("My windows are dirty")
    assert topic == "comp.os.ms-windows.misc"


def test_wrong_input_type(news_classifier):
    """
    Test classification of an invalid input type.

    Returns:
        None
    """

    with pytest.raises(TypeError):
        news_classifier.classify(123)


