# Create a cat command that works for both linux and windows
cat := $(if $(filter $(OS),Windows_NT),type,cat)

USERNAME := peterts
IMAGE = mockml
VERSION_FILE := version.txt
DEVELOPMENT_TAG := $(USERNAME)/$(IMAGE)-development:$(VERSION)
VERSION = $(shell $(cat) $(VERSION_FILE))

.PHONY: all build test app-version repo-login-aws tag-aws push-aws bump-version-patch bump-version-minor bump-version-major

all: build

# Bump patch version, e.g. 0.0.0 -> 0.0.1
bump-version-patch:
	python bump_version.py -f $(VERSION_FILE) patch

# Bump minor version, e.g. 0.0.5 -> 0.1.0
bump-version-minor:
	python bump_version.py -f $(VERSION_FILE) minor

# Bump major version, e.g. 0.1.7 -> 2.0.0
bump-version-major:
	python bump_version.py -f $(VERSION_FILE) major

# Get the current version
app-version:
	@echo $(VERSION)

# Build the docker image
build: bump-version-patch
	docker build -t $(USERNAME)/$(IMAGE):$(VERSION) -t $(USERNAME)/$(IMAGE):latest .

# Run the prediction api
run:
	docker run --rm -p 80:80 $(USERNAME)/$(IMAGE):latest

# Run shell in latest production image
shell:
	docker run --rm -it $(USERNAME)/$(IMAGE):latest bash

# Run tests in :latest image
test:
	docker run --rm  $(USERNAME)/$(IMAGE):latest pytest tests --verbose

# login to AWS-ECR
repo-login-aws:
	$(shell aws ecr get-login --no-include-email --region $(AWS_DEFAULT_REGION))

# Tag the image for the aws repo
tag-aws:
	docker tag $(USERNAME)/$(IMAGE):$(VERSION) $(AWS_ECR_REPO)/$(IMAGE):$(VERSION)
	docker tag $(USERNAME)/$(IMAGE):latest $(AWS_ECR_REPO)/$(IMAGE):latest

# Push the image to the aws repo
push-aws: tag-aws repo-login-aws
	docker push $(AWS_ECR_REPO)/$(IMAGE):$(VERSION)
	docker push $(AWS_ECR_REPO)/$(IMAGE):latest




