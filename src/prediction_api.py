from flask import Flask, request, jsonify
from news_classifier import NewsClassifier


# The flask app
app = Flask(__name__)

# The classifier
CLASSIFIER = None


def error(error_code, message, exception=None):
    """
    Create an error message

    Args:
        error_code (int): The HTTP error code.
        message (str): The message to send to the user.
        exception (Exception): An exception object.

    Returns:
        tuple: error message (string), error code (int)

    """

    response = {"error": message}
    if exception is not None:
        response.update({"exception": "{}: {}".format(type(exception).__name__, str(exception))})
    return jsonify(response), error_code


@app.route('/api/v1/predict', methods=['POST'])
def predict():
    """
    Predict the classes for a given text or list of texts. The input data
    must be on the format:

        { "texts": "Some text" }
        or:
        { "texts": ["Some text", "Some other text"] }

    Returns:
        tuple: predictions (json), status code (int)
    """

    print("Received example. Starting prediction.")
    data = request.get_json()
    if "texts" not in data:
        return error(400, "Expected input to contain item 'texts' - a string/list of strings")
    texts = data["texts"]

    if CLASSIFIER is None:
        return error(500, "Classifier has not been initialized")
    try:
        predicted_classes = CLASSIFIER.classify(texts)
        return jsonify({"predicted_classes": predicted_classes}), 200
    except TypeError as e:
        return error(400, "Invalid input type", e)


if __name__ == '__main__':
    CLASSIFIER = NewsClassifier()
    print("Starting the prediction API")
    app.run(host="0.0.0.0", port="80")
