import argparse
import re
import os


DEFAULT_FILENAME = "version.txt"
TYPE_PATCH = "patch"
TYPE_MINOR = "minor"
TYPE_MAJOR = "major"


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("type", help="the type of version bump",
                        type=str, choices=[TYPE_PATCH, TYPE_MINOR, TYPE_MAJOR])
    parser.add_argument("-f", "--filename", help="the file where the version is stored",
                        type=str, default=DEFAULT_FILENAME)
    args = parser.parse_args()

    type_indices = {TYPE_PATCH: 2, TYPE_MINOR: 1, TYPE_MAJOR: 0}

    if not os.path.exists(args.filename):
        with open(args.filename, 'w') as f:
            f.write("0.0.0")

    with open(args.filename, 'r') as f:
        version = f.read().strip()
        if not re.match("^\d+(\.\d+){2}$", version):
            raise ValueError("Invalid version number in version file {}".format(args.filename))

    version_list = [int(x) for x in version.split(".")]
    i = type_indices[args.type]
    version_list[i] += 1
    for j in range(i+1, len(version_list)):
        version_list[j] = 0
    old_version = version
    version = ".".join(str(x) for x in version_list)
    print("Version bumped from {} to {}".format(old_version, version))

    with open(args.filename, 'w') as f:
        f.write(version)




